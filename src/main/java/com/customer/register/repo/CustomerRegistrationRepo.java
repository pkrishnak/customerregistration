package com.customer.register.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.customer.register.entity.Customer;

@Repository
public interface CustomerRegistrationRepo extends JpaRepository<Customer, Integer> {

}
