package com.customer.register.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customer.register.entity.Customer;
import com.customer.register.exception.CustomerAlreadyRegisteredException;
import com.customer.register.exception.CustomerNotFoundException;
import com.customer.register.exception.CustomerRegistrationFailedException;
import com.customer.register.repo.CustomerRegistrationRepo;

@Service
public class CustomerRegistrationService {

	@Autowired
	private CustomerRegistrationRepo customerRegistrationRepo;

	public void registerCustomer(Customer customer) {
		if (customerRegistrationRepo.findById(customer.getCustomerID()).isPresent()) {
			throw new CustomerAlreadyRegisteredException();
		}
		customerRegistrationRepo.save(customer);
	}

	public List<Customer> fetchCustomer(Customer customer) {
		if (customer.getCustomerID() != null) {
			Optional<Customer> customerRetrieved = null;
			try {
				customerRetrieved = customerRegistrationRepo.findById(customer.getCustomerID());
			} catch (Exception ex) {
				throw new CustomerRegistrationFailedException();
			}
			if (customerRetrieved.isPresent()) {
				return Arrays.asList(customerRetrieved.get());
			} else {
				throw new CustomerNotFoundException();
			}
		} else {
			return customerRegistrationRepo.findAll();
		}
	}
}
