package com.customer.register.entity;

import java.io.Serializable;

public class Response implements Serializable {
	
	private static final long serialVersionUID = -3993610369624982688L;
	private String status;
	
	public Response(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Response [status=" + status + "]";
	}

}
