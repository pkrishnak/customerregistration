package com.customer.register.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customermaster")
public class Customer {

	@Id
	@Column(name = "customerid")
	private Integer customerID;

	@Column(name = "customername")
	private String customerName;

	@Column(name = "phone")
	private Long phone;

	@Column(name = "address")
	private String address;

	@Column(name = "dateofbirth")
	private Date dateOfBirth;

	@Column(name = "insertedtime", insertable = false)
	private String insertedTime;

	public Customer() {
	}

	public Customer(Integer customerID, String customerName, Long phone, String address, Date dateOfBirth) {
		super();
		this.customerID = customerID;
		this.customerName = customerName;
		this.phone = phone;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getInsertedTime() {
		return insertedTime;
	}

	public void setInsertedTime(String insertedTime) {
		this.insertedTime = insertedTime;
	}

	@Override
	public String toString() {
		return "Customer [customerID=" + customerID + ", customerName=" + customerName + ", phone=" + phone
				+ ", address=" + address + ", dateOfBirth=" + dateOfBirth + ", insertedTime=" + insertedTime + "]";
	}

}
