package com.customer.register.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customer.register.entity.Customer;
import com.customer.register.entity.Response;
import com.customer.register.service.CustomerRegistrationService;

@RestController
@RequestMapping("/customer")
public class CustomerRegistrationController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomerRegistrationController.class);

	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@PostMapping("/register")
	public ResponseEntity<Response> registerCustomer(@RequestBody Customer customer) {
		customerRegistrationService.registerCustomer(customer);
		LOG.info("Customer Registered Successfully..");
		return new ResponseEntity<Response>(new Response("Success"), HttpStatus.CREATED);
	}
	
	@PostMapping("/fetch")
	public ResponseEntity<List<Customer>> fetchCustomer(@RequestBody Customer customer) {		
		List<Customer> retrievedCustomer = customerRegistrationService.fetchCustomer(customer);	
		LOG.info("Customer Retrieved Successfully..");
		return new ResponseEntity<List<Customer>>(retrievedCustomer, HttpStatus.FOUND);
	}
}
