package com.customer.register.exception.handling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.customer.register.entity.Response;
import com.customer.register.exception.CustomerAlreadyRegisteredException;
import com.customer.register.exception.CustomerNotFoundException;
import com.customer.register.exception.CustomerRegistrationFailedException;

@RestControllerAdvice
public class CustomerRegistrationExceptionHandler {
	
	@ExceptionHandler(CustomerAlreadyRegisteredException.class)
	public ResponseEntity<Response> CustomerAlreadyRegisteredExceptionHandler(CustomerAlreadyRegisteredException ex) {
		return new ResponseEntity<Response>(new Response(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<Response> CustomerNotFoundExceptionHandler(CustomerNotFoundException	 ex) {
		return new ResponseEntity<Response>(new Response(ex.getMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(CustomerRegistrationFailedException.class)
	public ResponseEntity<Response> CustomerRegistrationFailedExceptionHandler(CustomerRegistrationFailedException ex) {
		return new ResponseEntity<Response>(new Response(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Response> RuntimeExceptionHandler(RuntimeException ex) {
		
		return new ResponseEntity<Response>(new Response("Failed"), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
