package com.customer.register.exception;

public class CustomerAlreadyRegisteredException extends RuntimeException {

	private static final long serialVersionUID = -7376296466833211257L;

	public CustomerAlreadyRegisteredException() {
		super("Customer already registered");
	}
}
