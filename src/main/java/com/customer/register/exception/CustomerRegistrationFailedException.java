package com.customer.register.exception;

public class CustomerRegistrationFailedException extends RuntimeException {

	private static final long serialVersionUID = 1773726609728520355L;
	
	public CustomerRegistrationFailedException() {
		super("Unable to perform the operation");
	}

}
