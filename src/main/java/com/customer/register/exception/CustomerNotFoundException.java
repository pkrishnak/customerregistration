package com.customer.register.exception;

public class CustomerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -5666030549729103123L;

	public CustomerNotFoundException() {
		super("Customer does not exists");
	}
}
